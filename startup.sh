java -jar wii-service-register/build/libs/wii-service-register-1.0-SNAPSHOT.jar > register.log  &
echo 'kill '$! > shutdown.sh
echo 'wii-service-register is starting'
sleep 10
java -jar wii-song-microservice/build/libs/wii-song-microservice-1.0-SNAPSHOT.jar > service.log &
echo 'kill '$! >> shutdown.sh
echo 'wii-song-microservice is starting'
sleep 50
java -jar wii-web-layer/build/libs/wii-web-layer-1.0-SNAPSHOT.jar > web.log &
echo 'kill '$! >> shutdown.sh
echo 'wii-web-layer is starting'
echo 'all apps started'
echo 'run shutdown.sh to terminate apps'
chmod +x shutdown.sh