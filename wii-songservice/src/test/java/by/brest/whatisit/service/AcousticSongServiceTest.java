package by.brest.whatisit.service;

import by.brest.whatisit.domain.Song;
import by.brest.whatisit.domain.SongDao;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Map;
import java.util.UUID;

/**
 * @author kiryl_chepeleu
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:service-song-service-test.xml")
public class AcousticSongServiceTest {

    private static final String NOT_EXISTED_FILE_NAME = "src/test/resources/no.txt";
    private static final String TXT_FILE_NAME = "src/test/resources/testFile.txt";
    private static final String MP3_FILE_NAME = "src/test/resources/test_song.mp3";
    private static final Logger LOG = LoggerFactory.getLogger(AcousticSongServiceTest.class);

    @Autowired
    private AcousticSongService acousticSongService;
    private SongDao songDao;

    private static File createTmpCopy(String fileName) throws IOException {

        File tempFile = File.createTempFile("tmp", "song");
        tempFile.deleteOnExit();
        Files.copy(FileSystems.getDefault().getPath(fileName), tempFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        return tempFile;
    }

    @Before
    public void setUp() {
        songDao = EasyMock.createMock(SongDao.class);
        acousticSongService.setSongDao(songDao);
    }

    @Test(expected = InvalidFingerPrintException.class)
    public void testUploadWithException() throws InvalidFingerPrintException, IOException {

        File file = createTmpCopy(TXT_FILE_NAME);
        String status = acousticSongService.uploadSong(file.getAbsolutePath(), "artistName", "trackName");
        LOG.debug(status);
    }

    @Test
    public void testUpload() throws InvalidFingerPrintException, IOException {

        File file = createTmpCopy(MP3_FILE_NAME);
        String status = acousticSongService.uploadSong(file.getAbsolutePath(), "artistName", "trackName");
        LOG.info(status);
        ObjectMapper objectMapper = new ObjectMapper();
        Map map = objectMapper.readValue(status, Map.class);
        Assert.assertEquals("ok", map.get("status"));
        Assert.assertFalse(file.exists());
    }

    @Test(expected = InvalidFingerPrintException.class)
    public void testRecognizeWithException() throws InvalidFingerPrintException, IOException, URISyntaxException {

        File file = createTmpCopy(TXT_FILE_NAME);
        String status = acousticSongService.uploadSong(file.getAbsolutePath(), "artistName", "trackName");
        LOG.debug(status);
    }

    @Test
    public void testRecognize() throws InvalidFingerPrintException, IOException, URISyntaxException {

        File file = createTmpCopy(MP3_FILE_NAME);
        String status = acousticSongService.getSongInfo(file.getAbsolutePath());
        LOG.info(status);
        ObjectMapper objectMapper = new ObjectMapper();
        Map map = objectMapper.readValue(status, Map.class);
        Assert.assertEquals("ok", map.get("status"));
        Assert.assertFalse(file.exists());
    }

    @Test(expected = InvalidFingerPrintException.class)
    public void testRecognizeWithIOException() throws InvalidFingerPrintException, IOException, URISyntaxException {

        String status = acousticSongService.getSongInfo(NOT_EXISTED_FILE_NAME);
        LOG.debug(status);
    }

    @Test(expected = InvalidFingerPrintException.class)
    public void testUploadWithIOException() throws InvalidFingerPrintException, IOException {

        String status = acousticSongService.uploadSong(NOT_EXISTED_FILE_NAME, "artistName", "trackName");
        LOG.debug(status);
    }

    @Test
    public void testSend() {

        try {
            acousticSongService.sendFingerPrintToAcoustid(
                    acousticSongService.getFingerPrintByFile(MP3_FILE_NAME)
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testFillSongInfoFromSongDao() throws IOException {

        final String json = "{\"status\": \"ok\", \"results\": [{\"score\": 1.0, \"id\": \"e5d257e4-0489-4eb2-9888-f95610e34954\"}]}";
        final String expectedJson = "{\"status\":\"ok\",\"results\":[{\"score\":1.0,\"id\":\"e5d257e4-0489-4eb2-9888-f95610e34954\",\"recordings\":[{\"title\":\"song\",\"artists\":[{\"name\":\"artist\"}]}]}]}";
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.readValue(json, ObjectNode.class);
        Song song = new Song(0L, "artist", "song", UUID.fromString("e5d257e4-0489-4eb2-9888-f95610e34954"));
        songDao.getSongByUuid(song.getUuid());
        EasyMock.expectLastCall().andReturn(song);
        EasyMock.replay(songDao);
        acousticSongService.fillSongInfoFromSongDao(objectNode);
        EasyMock.verify(songDao);
        Assert.assertEquals(expectedJson, objectNode.toString());
    }

    @Test
    public void testSaveSongInDataBase() {

        final String json = "{\"status\": \"ok\", \"submissions\": [{\"status\": \"imported\", \"id\": 153772596, \"result\": {\"id\": \"6e4edefb-de31-42d5-b97c-af2cd85148e1\"}}]}";
        final String artist = "artist";
        final String songName = "song";
        Song song = new Song(null, artist, songName, UUID.fromString("6e4edefb-de31-42d5-b97c-af2cd85148e1"));
        songDao.addSong(song);
        EasyMock.expectLastCall().andReturn(1L);
        EasyMock.replay(songDao);
        acousticSongService.saveSongInDataBase(json, artist, songName);
        EasyMock.verify(songDao);
    }
}
