package by.brest.whatisit.service;

import java.io.IOException;

/**
 * Interface for processing song files
 *
 * @author kiryl_chepeleu
 */
public interface SongService {

    /**
     * Abstract method that save song so it can be recognized
     *
     * @param fileName
     * @param artistName
     * @param trackName
     * @return upload status in json
     * @throws InvalidFingerPrintException
     * @throws IOException
     */
    String uploadSong(String fileName, String artistName, String trackName) throws InvalidFingerPrintException, IOException;

    /**
     * Method recognize song file
     *
     * @param fileName
     * @return recognition status in json
     * @throws InvalidFingerPrintException
     * @throws IOException
     */
    String getSongInfo(String fileName) throws InvalidFingerPrintException, IOException;
}
