package by.brest.whatisit.service;

import by.brest.whatisit.domain.Song;
import by.brest.whatisit.domain.SongDao;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

/**
 * @author kiryl_chepeleu
 */
public class AcousticSongService implements SongService {

    public static final String SUBMIT_URL = "http://api.acoustid.org/v2/submit?client={client}&user={user}&wait={wait}&duration={duration}&fingerprint={fingerprint}";
    public static final String LOOKUP_URL = "http://api.acoustid.org/v2/lookup?client={client}&meta=recordings&duration={duration}&fingerprint={fingerprint}";
    private static final Logger LOG = LoggerFactory.getLogger(AcousticSongService.class);
    private static String app;

    /**
     * Deploy cmd tool(fingerprint) to environment from jar to temp directory
     */
    static {

        try {
            LOG.info("start unpack binary util");
            String osName = System.getProperty("os.name").toLowerCase();
            String osArch = System.getProperty("os.arch").toLowerCase();
            app = String.format("fpcalc/fpcalc-%s-%s", osName, osArch);
            LOG.info("load inputStream from resources");
            InputStream stream = AcousticSongService.class.getClassLoader().getResourceAsStream(app);
            LOG.info("create temp file");
            File tempFile = File.createTempFile("fcalc", "tmp");
            app = tempFile.getAbsolutePath();
            tempFile.deleteOnExit();
            FileOutputStream tmp = new FileOutputStream(new File(app));
            LOG.info("copying file");
            IOUtils.copyLarge(stream, tmp);
            IOUtils.closeQuietly(stream);
            IOUtils.closeQuietly(tmp);
            if (tempFile.setExecutable(true)) {
                LOG.error("tool's file can not set executable");
            }
            LOG.info("finish unpack binary util");
        } catch (IOException e) {
            LOG.error("PLATFORM DON'T SUPPORT");
            e.printStackTrace();
        }
    }

    private final RestTemplate restTemplate = new RestTemplate();
    @Value("${url.client}")
    private String client;
    @Value("${url.user}")
    private String user;
    @Value("${url.wait}")
    private String wait;
    @Autowired
    private SongDao songDao;

    @Override
    public String uploadSong(String fileName, String artistName, String trackName) throws InvalidFingerPrintException, IOException {

        LOG.info("Method uploadSong filename={} artistName={} trackName={}"
                , fileName
                , artistName
                , trackName
        );
        Properties fingerPrint = getFingerPrintByFile(fileName);
        LOG.info("Method uploadSong fingerPrint={}", fingerPrint.toString());
        if (fingerPrint.size() != 3) {
            LOG.error("Bad fingerprint, check type file");
            throw new InvalidFingerPrintException();
        }
        deleteFile(fileName);
        String status = sendFingerPrintToAcoustid(fingerPrint);
        return saveSongInDataBase(status, artistName, trackName);
    }

    public String saveSongInDataBase(String string, String artistName, String trackName) {

        LOG.info("JsonResponse: {}", string);
        Song song = new Song();
        song.setSongName(trackName);
        song.setArtistName(artistName);
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = null;
        try {
            jsonNode = objectMapper.readValue(string, JsonNode.class);
            String uuid = jsonNode.get("submissions").get(0).get("result").get("id").asText();
            song.setUuid(UUID.fromString(uuid));
            songDao.addSong(song);
        } catch (IOException e) {
            LOG.error("Method saveSongInDataBase: ", e);
        }
        return string;
    }

    public String sendFingerPrintToAcoustid(Properties fingerPrint) {

        LOG.info("Method sendFingerPrintToAcoustid: {}", fingerPrint.toString());
        Map<String, String> params = new HashMap<String, String>();
        params.put("client", client);
        params.put("user", user);
        params.put("wait", wait);
        params.put("duration", fingerPrint.get("DURATION").toString());
        params.put("fingerprint", fingerPrint.get("FINGERPRINT").toString());
        LOG.info("client {}", client);
        LOG.info("user {}", user);
        return restTemplate.getForObject(SUBMIT_URL, String.class, params);
    }

    @Override
    public String getSongInfo(String fileName) throws InvalidFingerPrintException, IOException {

        LOG.info("Method getSongInfo fileName={}", fileName);
        Properties fingerPrint = getFingerPrintByFile(fileName);
        if (fingerPrint.size() != 3) {
            LOG.error("Bad fingerprint, check type file");
            throw new InvalidFingerPrintException();
        }
        deleteFile(fileName);
        ObjectNode objectNode = getSongInfoFromAcousticByFingerPrint(fingerPrint);
        fillSongInfoFromSongDao(objectNode);
        return objectNode.toString();
    }

    public ObjectNode fillSongInfoFromSongDao(ObjectNode objectNode) {

        ObjectMapper objectMapper = new ObjectMapper();
        for (JsonNode node : objectNode.get("results")) {
            if (!node.has("recordings")) {
                UUID uuid = UUID.fromString(node.get("id").asText());
                Song song = songDao.getSongByUuid(uuid);
                if (song != null) {
                    ObjectNode result = (ObjectNode) node;
                    ArrayNode recordings = result.putArray("recordings");
                    ObjectNode record = objectMapper.createObjectNode();
                    record.put("title", song.getSongName());
                    ObjectNode artist = objectMapper.createObjectNode();
                    artist.put("name", song.getArtistName());
                    record.putArray("artists").add(artist);
                    recordings.add(record);
                }
            }
        }
        return objectNode;
    }

    public Properties getFingerPrintByFile(String fileName) throws IOException {

        LOG.info("Method getFingerPrintByFile fileName={}", fileName);
        Process process = Runtime.getRuntime().exec(new String[]{app, fileName}, null);
        Properties properties = new Properties();
        properties.load(process.getInputStream());
        return properties;
    }

    public ObjectNode getSongInfoFromAcousticByFingerPrint(Properties fingerPrint) {

        Map<String, String> params = new HashMap<String, String>();
        params.put("client", client);
        params.put("duration", (String) fingerPrint.get("DURATION"));
        params.put("fingerprint", (String) fingerPrint.get("FINGERPRINT"));
        return restTemplate.getForObject(LOOKUP_URL, ObjectNode.class, params);
    }

    public void deleteFile(String fileName) {

        Path path = FileSystems.getDefault().getPath(fileName);
        try {
            Files.delete(path);
        } catch (IOException e) {
            LOG.error("IOException in deleteFile()", e);
        }
    }

    public void setSongDao(SongDao songDao) {
        this.songDao = songDao;
    }
}