package by.brest.whatisit.domain;

import java.util.UUID;

/**
 * Song is entity class
 *
 * @author kiryl_chepeleu
 */
public class Song {

    private Long songId;
    private String artistName = "unknown";
    private String songName = "unknown";
    /**
     * Hold the uuid of song on service acoustid.org
     */
    private UUID uuid;

    public Song() {
    }

    public Song(Long songId, String artistName, String songName, UUID uuid) {
        this.songId = songId;
        this.artistName = artistName;
        this.songName = songName;
        this.uuid = uuid;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public Long getSongId() {
        return songId;
    }

    public void setSongId(Long songId) {
        this.songId = songId;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    @Override
    public String toString() {
        return "Song{" +
                "songId=" + songId +
                ", artistName='" + artistName + '\'' +
                ", songName='" + songName + '\'' +
                ", uuid=" + uuid +
                '}';
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        Song song = (Song) object;

        if (songId != null ? !songId.equals(song.songId) : song.songId != null) return false;
        if (artistName != null ? !artistName.equals(song.artistName) : song.artistName != null) return false;
        if (songName != null ? !songName.equals(song.songName) : song.songName != null) return false;
        return uuid != null ? uuid.equals(song.uuid) : song.uuid == null;
    }

    @Override
    public int hashCode() {
        int result = songId != null ? songId.hashCode() : 0;
        result = 31 * result + (artistName != null ? artistName.hashCode() : 0);
        result = 31 * result + (songName != null ? songName.hashCode() : 0);
        result = 31 * result + (uuid != null ? uuid.hashCode() : 0);
        return result;
    }
}
