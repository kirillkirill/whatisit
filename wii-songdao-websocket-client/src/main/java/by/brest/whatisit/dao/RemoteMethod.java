package by.brest.whatisit.dao;

import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.sockjs.client.RestTemplateXhrTransport;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.util.ArrayList;

/**
 * Created by kiryl_chepeleu on 4.3.16.
 */
public class RemoteMethod {

    private static SockJsClient sockJsClient;

    static {
        ArrayList<Transport> transports = new ArrayList<>(2);
        transports.add(new WebSocketTransport(new StandardWebSocketClient()));
        transports.add(new RestTemplateXhrTransport());
        sockJsClient = new SockJsClient(transports);
    }

    private String url;
    private GenericObjectPoolConfig genericObjectPoolConfig;
    private ObjectPool<RemoteExecutor> pool;

    public RemoteMethod(final String url, GenericObjectPoolConfig genericObjectPoolConfig) {

        this.url = url;
        this.genericObjectPoolConfig = genericObjectPoolConfig;
        BasePooledObjectFactory<RemoteExecutor> basePooledObjectFactory = new BasePooledObjectFactory<RemoteExecutor>() {
            @Override
            public RemoteExecutor create() throws Exception {
                return new RemoteExecutor(sockJsClient, url);
            }

            @Override
            public PooledObject<RemoteExecutor> wrap(RemoteExecutor obj) {
                return new DefaultPooledObject<>(obj);
            }
        };
        pool = new GenericObjectPool<>(
                basePooledObjectFactory,
                genericObjectPoolConfig
        );
    }

    public String call(String message) {

        String result = null;
        try {
            RemoteExecutor remoteExecutor = pool.borrowObject();
            result = remoteExecutor.sendMessage(message);
            pool.returnObject(remoteExecutor);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
