package by.brest.whatisit.dao;

import by.brest.whatisit.domain.Song;
import by.brest.whatisit.domain.SongDao;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by kiryl_chepeleu on 4.3.16.
 */
public class SongDaoClient implements SongDao {

    private final static Logger LOG = LoggerFactory.getLogger(SongDaoClient.class);

    @Autowired
    private Map<String, RemoteMethod> methodMap;

    @Override
    public Long addSong(Song song) {

        LOG.info("Method addSong: {}", song);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String result = methodMap.get("addSong").call(objectMapper.writeValueAsString(song));
            Long id = Long.valueOf(result);
            song.setSongId(id);
            return id;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Song> getSongsList() {

        LOG.info("Method getSongsList");
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(methodMap.get("getSongsList").call("get"), List.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Song getSongById(long id) {

        LOG.info("Method getSongById: {}", id);
        String result = methodMap.get("getSongById").call(Long.toString(id));
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(result, Song.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Song getSongByUuid(UUID uuid) {

        LOG.info("Method getSongByUuid: {}", uuid);
        String result = methodMap.get("getSongByUuid").call(uuid.toString());
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(result, Song.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setMethodMap(Map<String, RemoteMethod> methodMap) {
        this.methodMap = methodMap;
    }
}
