package by.brest.whatisit.dao;

import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import org.springframework.web.socket.sockjs.client.SockJsClient;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Semaphore;

/**
 * Created by kiryl_chepeleu on 4.3.16.
 */
public class RemoteExecutor {

    private WebSocketSession webSocketSession;
    private Semaphore semaphoreForWaitingResponse = new Semaphore(1);
    private String returnValue;

    private WebSocketHandler webSocketHandler = new TextWebSocketHandler() {
        @Override
        protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
            returnValue = message.getPayload().toString();
            semaphoreForWaitingResponse.release();
        }
    };

    public RemoteExecutor(SockJsClient sockJsClient, String url) {

        try {
            this.webSocketSession = sockJsClient.doHandshake(webSocketHandler, url).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public String sendMessage(String message) {

        try {
            semaphoreForWaitingResponse.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            webSocketSession.sendMessage(new TextMessage(message));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            semaphoreForWaitingResponse.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        semaphoreForWaitingResponse.release();
        return returnValue;
    }

}
