var lr = require('tiny-lr'),
    gulp = require('gulp'),
    livereload = require('gulp-livereload'),
    csso = require('gulp-csso'),
    imagemin = require('gulp-imagemin'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    connect = require('connect'),
    server = lr(),
    htmlmin = require('gulp-htmlmin');

gulp.task('css', function() {
    gulp.src([
        './bower_components/bootstrap/dist/css/bootstrap.css'
        ,'./src/assets/css/*.css'
    ])
    .pipe(concat('screen.css'))
    .pipe(gulp.dest('./build/public/css/'))
    .pipe(csso())
    .pipe(gulp.dest('./build/resources/main/resources/css/'))
    .pipe(livereload(server));
});

gulp.task('html', function() {
    gulp.src(['./src/assets/html/*.html'])
    .pipe(gulp.dest('./build/public/'))
    .pipe(htmlmin({
        collapseWhitespace: true
    }))
    .pipe(gulp.dest('./build/resources/main/resources/'))
    .pipe(livereload(server));
}); 

gulp.task('js', function() {
    gulp.src([
            './bower_components/jquery/dist/jquery.js'
            ,'./bower_components/bootstrap/dist/js/bootstrap.js'
            ,'./bower_components/angular/angular.js'
            ,'./bower_components/angular-route/angular-route.js'
            ,'./src/assets/js/index.js'
    ])
    .pipe(concat('index.js'))
    .pipe(gulp.dest('./build/public/js'))
    .pipe(concat('index.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./build/resources/main/resources/js'))
    .pipe(livereload(server));

    gulp.src([
            './src/assets/js/recorderjs/recorder.js'
            ,'./src/assets/js/recorderjs/recorderWorker.js'
            ,'./src/assets/js/main.js'
            ,'./src/assets/js/audiodisplay.js'
    ])
    .pipe(gulp.dest('./build/public/js'))
    .pipe(uglify())
    .pipe(gulp.dest('./build/resources/main/resources/js'))
    .pipe(livereload(server));
});

gulp.task('images', function() {
    gulp.src('./src/assets/img/**/*')
    .pipe(imagemin())
    .pipe(gulp.dest('./build/public/img'))
    .pipe(gulp.dest('./build/resources/main/resources/img'))
});

gulp.task('http-server', function() {
    connect()
        .use(require('connect-modrewrite')(['^/test/(.*)$ http://localhost:2223/$1 [P]']))
        .use(require('connect-livereload')())
        .use(connect.static('./build/public'))
        .listen('9000');
    console.log('Server listening on http://localhost:9000');
});

gulp.task('default', function() {
    gulp.run('css');
    gulp.run('html');
    gulp.run('images');
    gulp.run('js');

    server.listen(35729, function(err) {
        if (err) return console.log(err);

        gulp.watch('src/assets/css/**/*.css', function() {
            gulp.run('css');
        });
        gulp.watch('src/assets/html/**/*.html', function() {
            gulp.run('html');
        });
        gulp.watch('src/assets/img/**/*', function() {
            gulp.run('images');
        });
        gulp.watch('src/assets/js/**/*', function() {
            gulp.run('js');
        });
    });
    gulp.run('http-server');
});

gulp.task('build', function() {
    gulp.run('css');
    gulp.run('html');
    gulp.run('images');
    gulp.run('js');
});