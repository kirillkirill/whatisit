var myApp = angular.module('myApp', ['ngRoute']);

myApp.config(['$routeProvider', function($routeProvider) {
    $routeProvider.
    when('/recognize', {
        templateUrl: 'recognize.htm',
        controller: 'recognizeCtrl'
    }).

    when('/recognizeByFile', {
        templateUrl: 'recognizeByFile.htm',
        controller: 'recognizeCtrl'
    }).

    when('/upload', {
        templateUrl: 'upload.htm',
        controller: 'uploadCtrl'
    }).

    otherwise({
        redirectTo: '/recognize'
    });
}]);

myApp.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                    console.log('file change');
                    console.log(element[0].files[0]);
                });
            });
        }
    };
}]);

myApp.service('fileUpload', ['$http', function ($http) {
    this.recognizeFileToUrl = function(file, uploadUrl){
        var fd = new FormData();
        var songs = {
            time: new Date(),
            buttonClass: "pulse"
        }
        fd.append('file', file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(function(response){
            console.log(response)
            songs.buttonClass = undefined
            songs.response = response
        },function(response){
            console.log(response)
            songs.buttonClass = undefined
            songs.response = response
        });
        return songs
    }
    this.uploadFileToUrl = function(file, song, artist, uploadUrl){
        var fd = new FormData();
        var uploadedSong = {
            time: new Date(),
            songName: song,
            artistName: artist,
            buttonClass: "pulse"
        }
        fd.append('file', file);
        fd.append('artistName', artist);
        fd.append('songName', song);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(function(response){
            console.log(response)
            uploadedSong.buttonClass = undefined
            uploadedSong.response = response
        },function(response){
            console.log(response)
            uploadedSong.buttonClass = undefined
            uploadedSong.response = response
        });
        return uploadedSong
    }
}]);
var uploadedSongs = []
myApp.controller('uploadCtrl', ['$scope', 'fileUpload', function($scope, fileUpload){
    $scope.startDate = new Date()
    $scope.state = "upload"
    $scope.uploadedSongs = uploadedSongs;

    $scope.uploadFile = function(){
        var file = $scope.toUploadFile;

        console.log('file is ' );
        console.dir(file);

        var uploadUrl = "/test/upload";
        var result = fileUpload.uploadFileToUrl(file, $scope.songNameToUpload, $scope.artistNameToUpload, uploadUrl);
        $scope.uploadedSongs.splice(0, 0, result);
    };
    $scope.getTitle = function(string){
        if(string){
            return string
        }else{
            return '???'
        }
    }
}]);
var recognizedSongs = []
myApp.controller('recognizeCtrl', ['$scope', 'fileUpload', '$interval', function($scope, fileUpload, $interval){
    console.log('create')
    $scope.startDate = new Date()
    $scope.state = "recognize"
    $scope.recognizedSongs = recognizedSongs;
    $scope.recording = false;
    $scope.byFile = false

    $scope.recognizeFile = function(){
        var file = $scope.myFile;

        console.log('file is ' );
        console.dir(file);

        var uploadUrl = "/test/recognize";
        var result = fileUpload.recognizeFileToUrl(file, uploadUrl);
        $scope.recognizedSongs.splice(0, 0, result);
    };

    $scope.getTitle = function(string){
        if(string){
            return string
        }else{
            return '???'
        }
    }

    $scope.sendRecordBlob = function(){
        if(window.lastBlob){
            if(window.lastBlob.size > 1000000){
                var file = window.lastBlob;
                console.log(file)
                var uploadUrl = "/test/recognize";
                var result = fileUpload.recognizeFileToUrl(file, uploadUrl);
                $scope.recognizedSongs.splice(0, 0, result);
            } else {
                $scope.recognizedSongs.splice(0, 0, {
                    response: {
                        statusText: 'very short part'
                    }
                });
            }
            window.lastBlob = null
        }
    }

    $interval(function(){
        $scope.sendRecordBlob();
    },100);

}]);