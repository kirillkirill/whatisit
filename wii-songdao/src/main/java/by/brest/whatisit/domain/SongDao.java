package by.brest.whatisit.domain;

import java.util.List;
import java.util.UUID;

/**
 * Interface to access some data store
 * Serve for save and load song information about song
 *
 * @author kiryl_chepeleu
 */
public interface SongDao {
    /**
     * Save song in song store
     *
     * @param song
     * @return id of saved song
     */
    Long addSong(Song song);

    /**
     * Load from store all song records
     *
     * @return all songs as List
     */
    List<Song> getSongsList();

    /**
     * Load from store song by id
     *
     * @param id
     * @return song, or null if not exist
     */
    Song getSongById(long id);

    /**
     * Load from store song by uuid
     *
     * @param uuid
     * @return song, or null if not exist
     */
    Song getSongByUuid(UUID uuid);
}
