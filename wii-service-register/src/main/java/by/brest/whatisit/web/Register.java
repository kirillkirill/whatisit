package by.brest.whatisit.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author kiryl_chepeleu
 */
@SpringBootApplication
@EnableEurekaServer
public class Register {
    
    public static void main(String[] args) {

        System.setProperty("spring.config.name", "register");
        SpringApplication.run(Register.class, args);
    }
}
