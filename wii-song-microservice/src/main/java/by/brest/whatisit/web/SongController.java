package by.brest.whatisit.web;

import by.brest.whatisit.service.InvalidFingerPrintException;
import by.brest.whatisit.service.SongService;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author kiryl_chepeleu on 11.2.16.
 */
@RestController
public class SongController {

    private final static Logger LOG = LoggerFactory.getLogger(SongController.class);

    @Autowired
    private SongService songService;

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> upload(@RequestParam("artistName") String artistName,
                                         @RequestParam("songName") String songName,
                                         @RequestParam("file") MultipartFile file) throws IOException {

        LOG.info("artistName={} songName={} fileSize={} fileName={} originalFilename={}"
                , artistName
                , songName
                , file.getSize()
                , file.getName()
                , file.getOriginalFilename()
        );
        File tempFile = saveFileToTmpDirectory(file);
        try {
            return new ResponseEntity<String>(
                    songService.uploadSong(tempFile.getAbsolutePath(), artistName, songName)
                    , HttpStatus.OK
            );
        } catch (InvalidFingerPrintException e) {
            e.printStackTrace();
            return new ResponseEntity<String>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
        }
    }

    @RequestMapping(value = "/recognize", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> recognize(@RequestParam("file") MultipartFile file) throws IOException {

        LOG.info("fileSize={} fileName={} originalFilename={}"
                , file.getSize()
                , file.getName()
                , file.getOriginalFilename()
        );
        File tempFile = saveFileToTmpDirectory(file);
        try {
            return new ResponseEntity<String>(
                    songService.getSongInfo(tempFile.getAbsolutePath())
                    , HttpStatus.OK
            );
        } catch (InvalidFingerPrintException e) {
            e.printStackTrace();
            return new ResponseEntity<String>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
        }
    }

    private File saveFileToTmpDirectory(MultipartFile file) throws IOException {

        File tempFile = File.createTempFile("wii-service", "music-file");
        tempFile.deleteOnExit();
        InputStream inputStream = file.getInputStream();
        FileOutputStream fileOutputStream = new FileOutputStream(tempFile);
        IOUtils.copyLarge(inputStream, fileOutputStream);
        IOUtils.closeQuietly(inputStream);
        IOUtils.closeQuietly(fileOutputStream);
        return tempFile;
    }
}
