package by.brest.whatisit.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.websocket.WebSocketMessagingAutoConfiguration;
import org.springframework.boot.context.embedded.MultipartConfigFactory;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

import javax.servlet.MultipartConfigElement;

/**
 * @author kiryl_chepeleu
 */
@EnableAutoConfiguration(exclude = {WebSocketMessagingAutoConfiguration.class})
@EnableDiscoveryClient
@ImportResource(value = "spring-microservice.xml")
@ComponentScan(basePackages = "by.brest.whatisit.web")
public class RestSongService {

    public static void main(String[] args) {

        System.setProperty("spring.config.name", "server");
        SpringApplication.run(RestSongService.class, args);
    }

    @Bean
    MultipartConfigElement multipartConfigElement() {

        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setMaxFileSize("50MB");
        factory.setMaxRequestSize("50MB");
        return factory.createMultipartConfig();
    }
}
