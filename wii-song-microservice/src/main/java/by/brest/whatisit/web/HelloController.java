package by.brest.whatisit.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

/**
 * @author kiryl_chepeleu
 */
@RestController
public class HelloController {

    private static final int ID = new Random().nextInt(100);

    @RequestMapping("/hello")
    public String home() {
        return "Hello world " + ID;
    }
}
