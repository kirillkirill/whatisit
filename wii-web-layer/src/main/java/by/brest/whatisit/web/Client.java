package by.brest.whatisit.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.embedded.MultipartConfigFactory;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

import javax.servlet.MultipartConfigElement;

/**
 * @author kiryl_chepeleu
 */
@EnableAutoConfiguration
@EnableDiscoveryClient
@EnableZuulProxy
public class Client {

    public static void main(String[] args) {

        System.setProperty("spring.config.name", "client");
        SpringApplication.run(Client.class, args);
    }

    @Bean
    MultipartConfigElement multipartConfigElement() {

        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setMaxFileSize("50MB");
        factory.setMaxRequestSize("50MB");
        return factory.createMultipartConfig();
    }
}