package by.brest.whatisit.dao.handlers;

import by.brest.whatisit.domain.Song;
import by.brest.whatisit.domain.SongDao;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

/**
 * Created by kiryl_chepeleu on 3.3.16.
 */
public class AddSongHandler extends TextWebSocketHandler {

    private final static Logger LOG = LoggerFactory.getLogger(AddSongHandler.class);

    @Autowired
    private SongDao songDao;

    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {

        LOG.info("AddSongHandler handle message={}", message);
        ObjectMapper objectMapper = new ObjectMapper();
        Song song = objectMapper.readValue(message.getPayload().toString(), Song.class);
        Long id = songDao.addSong(song);
        session.sendMessage(new TextMessage(String.valueOf(id)));
    }
}
