package by.brest.whatisit.dao.handlers;

import by.brest.whatisit.domain.Song;
import by.brest.whatisit.domain.SongDao;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.util.List;

/**
 * Created by kiryl_chepeleu on 3.3.16.
 */
public class GetSongListHandler extends TextWebSocketHandler {

    private final static Logger LOG = LoggerFactory.getLogger(GetSongListHandler.class);

    @Autowired
    private SongDao songDao;

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {

        LOG.info("GetSongListHandler handle message={}", message);
        List<Song> songsList = songDao.getSongsList();
        ObjectMapper objectMapper = new ObjectMapper();
        session.sendMessage(new TextMessage(objectMapper.writeValueAsString(songsList)));
    }
}
