package by.brest.whatisit.dao.handlers;

import by.brest.whatisit.domain.Song;
import by.brest.whatisit.domain.SongDao;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.util.UUID;

/**
 * Created by kiryl_chepeleu on 3.3.16.
 */
public class GetSongByUuidHandler extends TextWebSocketHandler {

    private final static Logger LOG = LoggerFactory.getLogger(GetSongByUuidHandler.class);

    @Autowired
    private SongDao songDao;

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {

        LOG.info("GetSongByUuidHandler handle message={}", message);
        ObjectMapper objectMapper = new ObjectMapper();
        UUID uuid = UUID.fromString(message.getPayload());
        Song song = songDao.getSongByUuid(uuid);
        session.sendMessage(new TextMessage(objectMapper.writeValueAsString(song)));
    }
}
