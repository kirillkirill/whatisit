package by.brest.whatisit.domain;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * Created by kiryl_chepeleu on 19.2.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring-dao-jdbc.xml"})
public class TestSongDaoJdbc {

    @Autowired
    SongDao songDao;

    @Test
    public void test() {

        Random random = new Random();
        List<Song> expected = new LinkedList<Song>();
        final int n = 10;

        for (int i = 0; i < n; i++) {
            Song song = new Song();
            song.setArtistName(String.valueOf(random.nextLong()));
            song.setSongName(String.valueOf(random.nextLong()));
            song.setUuid(UUID.randomUUID());
            songDao.addSong(song);
            expected.add(song);
            Assert.assertEquals(song, songDao.getSongById(song.getSongId()));
            Song songByUuid = songDao.getSongByUuid(song.getUuid());
            Assert.assertEquals(song, songByUuid);
            Song songById = songDao.getSongById(song.getSongId());
            Assert.assertEquals(song, songById);
        }

        List<Song> actual = songDao.getSongsList();

        for (int i = 0; i < n; i++) {
            Assert.assertEquals(expected.get(i), actual.get(i));
        }

        Song song = new Song();
        song.setArtistName(String.valueOf(random.nextLong()));
        song.setSongName(String.valueOf(random.nextLong()));
        song.setUuid(UUID.randomUUID());
        Assert.assertNotNull(songDao.addSong(song));
        Assert.assertNull(songDao.addSong(song));

        Assert.assertNull(songDao.getSongByUuid(UUID.randomUUID()));
        Assert.assertNull(songDao.getSongById(-1L));
    }
}
