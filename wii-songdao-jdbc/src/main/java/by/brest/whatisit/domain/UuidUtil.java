package by.brest.whatisit.domain;

import java.nio.ByteBuffer;
import java.util.UUID;

/**
 * Created by kiryl_chepeleu on 23.2.16.
 */
public final class UuidUtil {

    private UuidUtil() {
    }

    public static byte[] uuidToBytes(UUID uuid) {
        return ByteBuffer.allocate(16)
                .putLong(uuid.getMostSignificantBits())
                .putLong(uuid.getLeastSignificantBits())
                .array();
    }

    public static UUID bytesToUuid(byte[] bytes) {

        ByteBuffer buffer = ByteBuffer.allocate(16);
        buffer.put(bytes);
        buffer.flip();
        return new UUID(buffer.getLong(), buffer.getLong());
    }
}
