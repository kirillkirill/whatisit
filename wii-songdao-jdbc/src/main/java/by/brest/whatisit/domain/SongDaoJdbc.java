package by.brest.whatisit.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

/**
 * @author kiryl_chepeleu
 */
public class SongDaoJdbc implements SongDao {

    private static final Logger LOG = LoggerFactory.getLogger(SongDaoJdbc.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public Long addSong(Song song) {

        LOG.info("Method addSong: {}", song.toString());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("artistName", song.getArtistName());
        mapSqlParameterSource.addValue("songName", song.getSongName());
        mapSqlParameterSource.addValue("acousticUuid", UuidUtil.uuidToBytes(song.getUuid()));
        try {
            namedParameterJdbcTemplate.update(
                    "INSERT INTO song (artistName,songName,acousticUuid)values(:artistName,:songName,:acousticUuid)",
                    mapSqlParameterSource,
                    keyHolder
            );
            song.setSongId(keyHolder.getKey().longValue());
            return song.getSongId();
        } catch (DuplicateKeyException exception) {
            LOG.error("Song already exits {}", song);
        }
        return null;
    }

    @Override
    public List<Song> getSongsList() {

        LOG.info("Method getSongList");
        return jdbcTemplate.query("SELECT * FROM song", new SongRowMapper());
    }

    @Override
    public Song getSongById(long id) {

        LOG.info("Method getSongById: {}", id);
        try {
            return jdbcTemplate.queryForObject("SELECT * FROM song WHERE songId = ?", new SongRowMapper(), id);
        } catch (EmptyResultDataAccessException exception) {
            LOG.error("Song don't exist {}", id);
        }
        return null;
    }

    @Override
    public Song getSongByUuid(UUID uuid) {

        LOG.info("Method getSongByUuid: {}", uuid);
        try {
            return jdbcTemplate.queryForObject("SELECT * FROM song WHERE acousticUuid = ?", new SongRowMapper(), UuidUtil.uuidToBytes(uuid));
        } catch (EmptyResultDataAccessException exception) {
            LOG.error("Song don't exist {}", uuid);
        }
        return null;
    }
}

class SongRowMapper implements RowMapper<Song> {

    @Override
    public Song mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Song(
                rs.getLong("songId"),
                rs.getString("artistName"),
                rs.getString("songName"),
                UuidUtil.bytesToUuid(rs.getBytes("acousticUuid"))
        );
    }
}